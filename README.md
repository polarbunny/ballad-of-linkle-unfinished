# Ballad of Linkle

An archive of the unfinished files for the discontinued Ballad of Linkle mod for The Legend of Zelda: Breath of the Wild.

Meant to be combined with TitleBG.pack without gate check "fix". I'll check to ensure it's up to date with latest  Linkle Mod update soon, until then this might work for now.
https://drive.google.com/file/d/1olJk0t3hvS7DPVPIN455Fl2n981QbWDB/view?usp=sharing

Requires compiling files in this project and modifying msbt to match.